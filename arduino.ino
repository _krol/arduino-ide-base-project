/*
 * _krol
 * http://krolg.info - https://bitbucket.org/_krol/
 */

/* ============= */
/* Serial Config */
#define SERIAL 1
#if SERIAL
  #define BAUD 9600
#endif
/* ============= */

/* ======== */
/* #Defines
/* ======== */
#define DEBUG_PIN 13

/* ===================== */
/* Function Declarations */

/* ===================== */


/** Setup function. Code in here runs once during startup */
void setup() 
{  
  /* Set up Debug Pin*/
  pinMode(DEBUG_PIN, OUTPUT);
  
  /* Set up Serial*/
  #if SERIAL
    Serial.begin(BAUD);
    while(!Serial) { /* wait for Serial connection */ }
    Serial.println("Ready");
  #endif
}

/** Main loop. Code in here runs indefiniately */
void loop() 
{
  
}
