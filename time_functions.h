/*
 * Time functions
 * _krol, 2015
 * http://krolg.info - https://bitbucket.org/_krol/
 */

/* Feedback gives us visual feedback on the DEBUG_PIN */
#define FEEDBACK 1
#define FEEDBACK_PIN 13

/* Note: all these use delay(). They block until complete. */

void delaySeconds(int seconds);
void delayMinutes(int minutes);
void delayHours(int hours);
