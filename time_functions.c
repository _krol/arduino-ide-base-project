/*
 * Time functions
 * _krol, 2015
 * http://krolg.info - https://bitbucket.org/_krol/
 */
#include "time_functions.h"
#include "arduino.h"

static void feedback()
{
  digitalWrite(FEEDBACK_PIN, HIGH);
  delay(10);
  digitalWrite(FEEDBACK_PIN, LOW);
}

void delaySeconds(int seconds)
{
  int i;
  for(i=0; i<seconds; i++)
  {
    delay(1000); // 1000ms/s
  }
}

void delayMinutes(int minutes)
{
  int i;
  for(i=0; i<minutes; i++)
  {
    delaySeconds(10);
    feedback();
    delaySeconds(10);
    feedback();
    delaySeconds(10);
    feedback();
    delaySeconds(10);
    feedback();
    delaySeconds(10);
    feedback();
    delaySeconds(10);
  }
}

void delayHours(int hours)
{
  int i;
  for(i=0; i<hours; i++)
  {
    delayMinutes(60);
  }
}
